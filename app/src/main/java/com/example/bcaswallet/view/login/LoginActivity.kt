package com.example.bcaswallet.view.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcaswallet.databinding.ActivityLoginBinding
import com.example.bcaswallet.view.MainActivity
import com.example.bcaswallet.view.RegisterActivity

class LoginActivity : AppCompatActivity (){
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()

            if (email.isEmpty() && password.isEmpty()){
                Toast.makeText(this, "Please Insert Email and Password", Toast.LENGTH_SHORT).show()
            } else if (email.isEmpty()){
                Toast.makeText(this, "Please Insert Email", Toast.LENGTH_SHORT).show()
            }else if (password.isEmpty()){
                Toast.makeText(this, "Please Insert Password", Toast.LENGTH_SHORT).show()
            }else{
                intentTo(MainActivity::class.java)
            }
        }

        binding.btnRegister.setOnClickListener {
            intentTo(RegisterActivity::class.java)
        }
    }
    private fun intentTo(clazz: Class<*>){
        val intent = Intent(applicationContext, clazz)
        startActivity(intent)
    }
}