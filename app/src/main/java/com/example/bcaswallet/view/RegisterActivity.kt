package com.example.bcaswallet.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcaswallet.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.setOnClickListener {
            val name = binding.etNama.text.toString()
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()

            if (email.isEmpty() && password.isEmpty() && name.isEmpty()){
                Toast.makeText(this, "Please Insert Name, Email and Password", Toast.LENGTH_SHORT).show()
            } else if (name.isEmpty()){
                Toast.makeText(this, "Please Insert Name", Toast.LENGTH_SHORT).show()
            } else if (email.isEmpty()){
                Toast.makeText(this, "Please Insert Email", Toast.LENGTH_SHORT).show()
            } else if (password.isEmpty()){
                Toast.makeText(this, "Please Insert Password", Toast.LENGTH_SHORT).show()
            }else{
                intentTo(MainActivity::class.java)
            }
        }
    }
    private fun intentTo(clazz: Class<*>){
        val intent = Intent(applicationContext, clazz)
        startActivity(intent)
    }
}