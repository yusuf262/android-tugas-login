package com.example.bcaswallet.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcaswallet.databinding.ActivityMainBinding
import com.example.bcaswallet.view.login.LoginActivity

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogout.setOnClickListener {
            intentTo(LoginActivity::class.java)
        }
    }
    private fun intentTo(clazz: Class<*>){
        val intent = Intent(applicationContext, clazz)
        startActivity(intent)
    }
}